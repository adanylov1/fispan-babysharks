package com.fispan.hackathon.controller;

import com.fispan.hackathon.model.Employee;
import com.fispan.hackathon.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
public class EmployeesController
{
    private final EmployeeService employeeService;

    @GetMapping("/employees")
    public ResponseEntity<List<Employee>> getAllEmployees()
    {
        log.info("Requesting all employees...");
        List<Employee> employees = employeeService.getEmployees();
        log.info("Extracted {} employees", employees.size());
        return new ResponseEntity<>(employees, HttpStatus.ACCEPTED);
    }

    @GetMapping("/employees/{id}")
    public ResponseEntity<Employee> getAllEmployeesIds(@PathVariable String id)
    {
        Employee employee = employeeService.getEmployeeById(id);
        return new ResponseEntity<>(employee, HttpStatus.ACCEPTED);
    }

    @PostMapping("/employees/generate")
    public ResponseEntity<Void> generateEmployees()
    {
        log.info("Generating employees...");
        employeeService.generateEmployees();
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }



}
