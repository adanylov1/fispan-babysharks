package com.fispan.hackathon.controller;

import com.fispan.hackathon.model.PaymentRequest;
import com.fispan.hackathon.service.PaymentService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class PaymentController
{

    private final PaymentService paymentService;

    @PostMapping("/cards")
    public ResponseEntity<Void> initiateCards(@RequestBody PaymentRequest paymentRequest)
    {
        paymentService.initiatePaymentRequest(paymentRequest);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

}
