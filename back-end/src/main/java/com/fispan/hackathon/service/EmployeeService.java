package com.fispan.hackathon.service;

import com.fispan.hackathon.exception.ValidationException;
import com.fispan.hackathon.model.Employee;
import com.fispan.hackathon.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class EmployeeService
{
    private final EmployeeRepository employeeRepository;

    public void generateEmployees()
    {
        employeeRepository.saveAll(randomEmployees());
    }

    public List<Employee> getEmployees()
    {
        return employeeRepository.findAll();
    }

    public Employee getEmployeeById(String id)
    {
       return employeeRepository.findById(id).orElseThrow(() -> new ValidationException("No used"));
    }

//    public List<String> getEmployeesIds()
//    {
//        return employeeRepository.findAll();
//    }



    private List<Employee> randomEmployees()
    {
        List<Employee> employees = new ArrayList<>();

        employees.add(Employee.builder()
                .id("1111")
                .email("adanylov@fispan.com")
                .firstName("Anton")
                .lastName("Danylov")
                .phone("778-917-8592")
                .build());

        employees.add(Employee.builder()
                .id("2222")
                .email("aramirez@fispan.com")
                .firstName("Alfredo")
                .lastName("Ramirez")
                .phone("778-917-8592")
                .build());

        employees.add(Employee.builder()
                .id("3333")
                .email("sarguedas@fispan.com")
                .firstName("Sebastian")
                .lastName("Arguedas")
                .phone("778-917-8592")
                .build());

        employees.add(Employee.builder()
                .id("5555")
                .email("rali@fispan.com")
                .firstName("Rabie")
                .lastName("Ali")
                .phone("778-917-8592")
                .build());

        employees.add(Employee.builder()
                .id("4444")
                .email("dbhojak@fispan.com")
                .firstName("Dev")
                .lastName("Bhojak")
                .phone("778-917-8592")
                .build());

        log.info("Generated {} employees", employees.size());

        return employees;
    }

}
