package com.fispan.hackathon.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fispan.hackathon.dto.CardInfo;
import com.fispan.hackathon.dto.CreateCard;
import com.fispan.hackathon.util.CommonLinks;
import com.fispan.hackathon.util.RequestMethod;
import com.fispan.hackathon.util.RequestUtil;
import com.fispan.hackathon.util.Wrapper;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import static com.fispan.hackathon.config.Constants.PAYMENT_ACCOUNT_ID;

@Slf4j
@Service
public class RequestService
{

    private static final ObjectMapper mapper = new ObjectMapper();

    public <T, Y> void createRequest(CommonLinks commonLinks, Map<String, String> headers,
                               Wrapper<T> clazz, Y bodyObject)
    {
        URL url;
        try
        {
            url = new URL(commonLinks.getLink());

            HttpURLConnection con = (HttpURLConnection) url.openConnection();

            con.setRequestMethod("POST");
            headers.forEach(con::setRequestProperty);
            headers.put("Content-Length", String.valueOf(mapper.writeValueAsString(bodyObject).length()));


            if(con.getRequestMethod().equals(RequestMethod.POST.name()))
            {
                con.setDoOutput(true);

                try(OutputStream os = con.getOutputStream()) {
                    byte[] input = mapper.writeValueAsString(bodyObject).getBytes(StandardCharsets.UTF_8);
                    os.write(input, 0, input.length);
                    os.flush();
                }
            }


            log.info("Requesting url = [{}], method = [{}]", commonLinks.getLink(), commonLinks.getMethod());

            int responseCode = con.getResponseCode();
            String responseMessage = con.getResponseMessage();

            log.info("Response code {}", responseCode);
            log.info("Response message {}", responseMessage);

        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public <T> void createRequest(CommonLinks commonLinks, Wrapper<T> clazz, Object bodyObject)
    {
        createRequest(commonLinks, RequestUtil.defaultHeaders(), clazz, bodyObject);
    }

    public <T> void createRequest(CommonLinks commonLinks, Wrapper<T> clazz)
    {
        createRequest(commonLinks, RequestUtil.defaultHeaders(), clazz, StringUtils.EMPTY);
    }

}
