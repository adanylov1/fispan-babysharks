package com.fispan.hackathon.service;

import com.fispan.hackathon.dto.CardInfo;
import com.fispan.hackathon.dto.CreateCard;
import com.fispan.hackathon.dto.ResponseCard;
import com.fispan.hackathon.exception.ValidationException;
import com.fispan.hackathon.model.Employee;
import com.fispan.hackathon.model.PaymentRequest;
import com.fispan.hackathon.repository.EmployeeRepository;
import com.fispan.hackathon.repository.PaymentRequestRepository;
import com.fispan.hackathon.util.CommonLinks;
import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.fispan.hackathon.config.Constants.PAYMENT_ACCOUNT_ID;

@Service
@RequiredArgsConstructor
public class PaymentService
{

    private final PaymentRequestRepository paymentRequestRepository;

    private final CardService cardService;

    private final EmployeeRepository employeeRepository;

    private final MongoTemplate mongoTemplate;

    public void initiatePaymentRequest(PaymentRequest paymentRequest)
    {
        validatePaymentRequest(paymentRequest);

        List<String> employeeIds = paymentRequest.getEmployeeIds();

        List<Employee> employees = employeeRepository.findAllById(employeeIds);

        employees.forEach(employee -> makePaymentRequest(paymentRequest, employee));
    }

    private void validatePaymentRequest(PaymentRequest paymentRequest)
    {
        if(paymentRequest.getAmount() == 0)
        {
            throw new ValidationException("Amount cannot be 0");
        }

        if(paymentRequest.getEmployeeIds().isEmpty())
        {
            throw new ValidationException("At least one employee should be added");
        }

        if(paymentRequest.getMCCs().isEmpty())
        {
            throw new ValidationException("At least one MCC should be added");
        }
    }

    private void makePaymentRequest(PaymentRequest paymentRequest, Employee employee)
    {

        CreateCard createCard = CreateCard.builder()
                .amount(paymentRequest.getAmount())
                .paymentAccountId(PAYMENT_ACCOUNT_ID)
                .cardInfo(CardInfo.builder()
                        .employeeId(employee.getId())
                        .email(employee.getEmail())
                        .firstName(employee.getFirstName())
                        .lastName(employee.getLastName())
                        .phone(employee.getPhone())
                        .build())
                .effectiveUntil(paymentRequest.getEffectiveUntil())
                .build();

        cardService.postVirtualCard(createCard);
    }

}
