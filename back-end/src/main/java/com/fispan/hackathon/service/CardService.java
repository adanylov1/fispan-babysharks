package com.fispan.hackathon.service;

import com.fispan.hackathon.dto.CreateCard;
import com.fispan.hackathon.dto.ResponseCard;
import com.fispan.hackathon.dto.VirtualCard;
import com.fispan.hackathon.util.CommonLinks;
import com.fispan.hackathon.util.Wrapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CardService
{

    private final RequestService requestService;

    public void postVirtualCard(CreateCard createCardPayload)
    {
        requestService.createRequest(CommonLinks.CREATE_CARD, new Wrapper<>(ResponseCard.class), createCardPayload);
    }


}
