package com.fispan.hackathon.repository;

import com.fispan.hackathon.model.PaymentRequest;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentRequestRepository extends MongoRepository<PaymentRequest, String>
{
}
