package com.fispan.hackathon.exception;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ValidationExceptionModel
{

    private String message;

    private String timestamp;

}
