package com.fispan.hackathon.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@Controller
public class ExceptionHandlingController {

  
  // Specify name of a specific view that will be used to display the error:
  @ExceptionHandler({ValidationException.class})
  public String validationError() {

    return "databaseError";
  }

  // Total control - setup a model and return the view name yourself. Or
  // consider subclassing ExceptionHandlerExceptionResolver (see below).
  @ExceptionHandler(Exception.class)
  public ResponseEntity<ValidationExceptionModel> handleError(HttpServletRequest req, ValidationException ex) {
      ValidationExceptionModel response = ValidationExceptionModel.builder()
            .message(ex.getMessage())
            .timestamp(LocalDateTime.now().toString())
            .build();

      return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
  }
}