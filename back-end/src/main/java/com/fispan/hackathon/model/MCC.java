package com.fispan.hackathon.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class MCC
{

    private int id;

    private String mcc;

    @JsonProperty("edited_description")
    private String editedDescription;

    @JsonProperty("combined_description")
    private String combinedDescription;

    @JsonProperty("usda_description")
    private String usdaDescription;

    @JsonProperty("irs_description")
    private String irsDescription;

    @JsonProperty("irs_reportable")
    private String irsReportable;

}
