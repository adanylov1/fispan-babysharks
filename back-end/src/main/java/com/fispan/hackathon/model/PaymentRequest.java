package com.fispan.hackathon.model;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.MongoId;

import java.util.List;

@Document(collection = "payment_requests")
@Data
@Builder
public class PaymentRequest
{

    @MongoId
    private String id;

    private int amount;

    private List<String> employeeIds;

    private List<MCC> MCCs;

    private String pattern;

    private String effectiveUntil;

//    public String getId()
//    {
//        return id;
//    }
//
//    public int getAmount()
//    {
//        return amount;
//    }
//
//    public List<String> getEmployeeIds()
//    {
//        return employeeIds;
//    }
//
//    public List<MCC> getMCCs()
//    {
//        return MCCs;
//    }
//
//    public String getPattern()
//    {
//        return pattern;
//    }
}
