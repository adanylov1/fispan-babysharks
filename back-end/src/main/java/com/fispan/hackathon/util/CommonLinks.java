package com.fispan.hackathon.util;

import lombok.Getter;

@Getter
public enum CommonLinks
{
    CREATE_CARD("https://alpha-api.usbank.com/innovation/bank-node/caas/v1/vcard", RequestMethod.POST);

    private final String link;

    private final RequestMethod method;

    CommonLinks(String link, RequestMethod method)
    {
        this.link = link;
        this.method = method;
    }
}
