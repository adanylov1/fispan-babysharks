package com.fispan.hackathon.util;

import com.fispan.hackathon.config.Constants;

import java.util.HashMap;
import java.util.Map;

public class RequestUtil
{

    public static Map<String, String> defaultHeaders()
    {
        Map<String, String> headers = new HashMap<>();

        headers.put("Content-Type", "application/json");
        headers.put("Accept-Encoding", "gzip, deflate, br");
//        headers.put("Connection", "keep-alive");
        headers.put("Accept", "application/json");
        headers.put("Authorization", "Basic blMyS0pldjRKc05BclEzTnFES0RHVGhPZ1BBTDFGTXg6b1k4OEl0Q2l0RkpSY1NYdQ==");
//        headers.put("User-Agent", "PostmanRuntime/7.26.8");
        headers.put("Cookie", "bm_sz=C00DBFEA0351BA741E1EFB7987DA29B2~YAAQR3ZiaDWfN2J7AQAAXpHUYgzi0WaAB0iqakqA1OlGEP9SPRcC22RPxRBwgZ+QdlEBw2DphDbkVDPQOc24//fcQbOMhLubjXr9+7l8CRCHhlszDMSbJoHmb74/dfxzfKuod1VIOvSp8QQu/u5Exqu0S6wRIuqvTPT/eUoXgIhs3Jq7XOwT+ReXpZ6ERTQ=; _abck=58D351DB82CD5496E7488E60E2F1A252~-1~YAAQR3ZiaDafN2J7AQAAXpHUYgb8vNxQRevz5tDcGSFzwQXnU6+6rAyn2UpvRERr9Vns4vg6iOPkqbUXCQ+yeKKlby3YYjznyrB1OA4fgXUjlP0tPvQRCaVx8VldNfhAaLKFiref68CJEcxTPwH/qzN7Jjj6DAxELAVD788LjZ4ugSzcq/BdCZe3pus0dHvVZtSGy/U2RFnqPw4wt2X/FuNUU6KDgQH7dQPzteqJGxFfFUbnNwisc/tdJAeaftycA4JanEDPJM0warTS2dj8mpAF3dziWygFosS79I5ipTdbPFOU1pneUtbmMbXFCMbmBypHHijttYyJalOzmf8Aho1JctSQQ8i9KD+7OErv0pBzLxvlSGNuuPfXaXAWDESnf5I7LxSBKQ==~-1~-1~-1; ak_bmsc=B16A39922DA96AEC214FF65489386243~000000000000000000000000000000~YAAQR3ZiaDefN2J7AQAAXpHUYgzb8v48ZI0rOrE94MojKOaX+GGqLd+d7oc1plnvjnkLhVEiOGkI+ycKYcnBgU4eV6qLh8kCoXgAhilFuZcszSZgphw2SPi1ooY0tPRmGAAEAlX/q/tYNtCykfGaHKjw9geOKYLo6q0vTQ861xmmSZ2+FwcRA96EgXTvS+9tR0pzPCny8CZtXPIbxvdYAwAkSNG5bZatN3c/b/rJIrYj2Uc/BM9NrRs7wLNOEiOW+PTUj8PE+9VgnmoEHDzw5OVIIUmwAKdSNbNaVAjQ9XmNDsw/ujQOg8es41/VQzy/7y7Ax5tC4Ew9IsMCuj2bWLmuNebzgZ/jT/Y8xW7dJgVhByqpamSl2pD0JRc=; bm_sv=2D3891F656FD9905F5BF71CF17E20961~GQQqQbQ+QNgimdvjePjW9iHT4RYqD8oQhqF9dHpYfFGzS59WEPS9sqCcnK783WJlrtnk2fmiEIgyhteEmqldH8wU7k6UzQoSEC0d+qNj7eTmv4QBWJGkGCd2GbcMiS3fmjgP+6tlCSdVd3RanSObWE8ptGyVY1nC+ToXle4+nwM=");
        headers.put("Host", "174.7.185.244");

        return headers;
    }

    public static Map<String, String> addHeaders(Map<String, String> customHeaders)
    {
        Map<String, String> headers = defaultHeaders();
        headers.putAll(customHeaders);
        return headers;
    }

}
