package com.fispan.hackathon.util;

public enum RequestMethod
{
    GET,
    POST,
    DELETE
}
