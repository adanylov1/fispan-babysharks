package com.fispan.hackathon.util;

import lombok.Getter;

@Getter
public class Wrapper<T> {

    final Class<T> typeParameterClass;

    public Wrapper(Class<T> typeParameterClass) {
        this.typeParameterClass = typeParameterClass;
    }

    public void bar() {
        // you can access the typeParameterClass here and do whatever you like
    }
}