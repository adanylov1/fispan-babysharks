package com.fispan.hackathon.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder
public class CreateCard
{

    @JsonProperty("amount")
    private int amount;

    @JsonProperty("cardInfo")
    private CardInfo cardInfo;

    @JsonProperty("effectiveUntil")
    private String effectiveUntil;

    @JsonProperty("paymentAccountID")
    private String paymentAccountId;

    @JsonProperty("returnCVV")
    @Builder.Default
    private boolean returnCVV = true;

}
