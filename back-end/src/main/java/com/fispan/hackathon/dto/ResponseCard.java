package com.fispan.hackathon.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ResponseCard
{

    @JsonProperty("virtualCard")
    private VirtualCard virtualCard;

}
