package com.fispan.hackathon.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class VirtualCard
{

    @JsonProperty("ID")
    private String id;

    @JsonProperty("number")
    private String number;

    @JsonProperty("CVV")
    private String cvv;

    @JsonProperty("zip")
    private String zip;

    @JsonProperty("expirationDate")
    private String expirationDate;

}
